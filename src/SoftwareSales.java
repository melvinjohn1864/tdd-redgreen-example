
public class SoftwareSales {
	
	public double calculatePrice(int quantity) {
		
		if (quantity < 0) {
			return -1;
		}
		
		int subtotal = quantity * 99;
		double discount = 0;
		if (quantity >= 10 && quantity <= 19) {
			discount = subtotal * 0.20;
		}else if (quantity >= 20 && quantity <= 49) {
			discount = subtotal * 0.30;
		}else if (quantity >= 50 && quantity <= 99) {
			discount = subtotal * 0.40;
		}else if (quantity >= 100) {
			discount = subtotal * 0.50;
		}
		
		double finalPrice = subtotal - discount;
		return finalPrice;
	}

}
