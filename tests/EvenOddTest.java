import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EvenOddTest {
	
	EvenOdd e;

	@Before
	public void setUp() throws Exception {
		e = new EvenOdd();
	}

	@After
	public void tearDown() throws Exception {
	}
	
//	R1: Accepts 1 integer value, n
//	N > 1  
//	R2: If N < 1, then return false
//	R3: If n is even, return true
//	R4: If n is odd, return false


	@Test
	public void testEvenOddFunction() {
		assertEquals(true,e.isEven(20));
		
	}
	
	@Test
	public void testNLessthanOne() {
		int n = -999;
		assertEquals(false,e.isEven(n));
		
	}
	
	@Test
	public void testIsNEven() {
		int n = 30;
		assertEquals(true,e.isEven(n));
		
	}
	
	@Test
	public void testIsNOdd() {
		int n = 31;
		assertEquals(false,e.isEven(n));
		
	}
	

}
